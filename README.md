# Hannes Website

## How to install

1. `git clone https://gitlab.com/Rubinum/hannes-webapp.git`
2. cd hannes-webapp
3. composer install

If you want to run a develop server run: `bin/console server:start`.

To generate database schema use `bin/console doctrine:database:create && bin/console doctrine:schema:create`
<?php

namespace App\Controller;

use App\Entity\WeatherData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->render('homepage.html.twig');
    }

    /**
     * @Route("/statistics", name="statistics")
     */
    public function statistics(EntityManagerInterface $entityManager): Response
    {
        $statisticsRepository = $entityManager->getRepository(WeatherData::class);
        $statistics = $statisticsRepository->findAll();
        return $this->render('statistics.html.twig', ['statistics' => $statistics]);
    }

    /**
     * @Route("/statistics/input", name="input")
     */
    public function retrieveStatistics(Request $request, EntityManagerInterface $entityManager): Response
    {
        $parameterArray = $request->request->all();
        $statistic = WeatherData::fromRaspPiInput($parameterArray);
        $entityManager->persist($statistic);
        $entityManager->flush();

        return new Response();
    }
}

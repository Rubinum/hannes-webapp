<?php

namespace App\Controller;

use App\Entity\WeatherData;
use App\Repository\WeatherDataRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NavigationController extends AbstractController
{
    /**
     * @Route("/wetterstation", name="weatherData")
     */
    public function weatherData(WeatherDataRepository $weatherDataRepository): Response
    {
        $weatherData = $weatherDataRepository->findLatest();
        return $this->render('wetterdaten.html.twig', ['weatherData' => $weatherData]);
    }

    /**
     * @Route("/etechnik", name="electricalEngineering")
     */
    public function electricalEngineering(): Response
    {
        return $this->render('electrical_engineering.html.twig');
    }

    /**
     * @Route("/mathematik", name="mathematics")
     */
    public function mathematics(): Response
    {
        return $this->render('mathematics.html.twig');
    }

    /**
     * @Route("/impressum", name="impressum")
     */
    public function impressum(): Response
    {
        return $this->render('impressum.html.twig');
    }

    /**
     * @Route("/datenschutz", name="privacy")
     */
    public function privacy(): Response
    {
        return $this->render('privacy.html.twig');
    }
}

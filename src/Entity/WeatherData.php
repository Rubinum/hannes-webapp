<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="WeatherDataRepository")
 */
class WeatherData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="temperature", type="float", nullable=false)
     */
    private $temperature;

    /**
     * @ORM\Column(name="windchill", type="float", nullable=false)
     */
    private $windchill;

    /**
     * @ORM\Column(name="pressure", type="float", nullable=false)
     */
    private $pressure;

    /**
     * @ORM\Column(name="relative_humidity", type="float", nullable=false)
     */
    private $relativeHumidity;

    /**
     * @ORM\Column(name="wind_speed", type="float", nullable=false)
     */
    private $windSpeed;

    /**
     * @ORM\Column(name="wind_direction", type="integer", nullable=false)
     */
    private $windDirection;

    /**
     * @ORM\Column(name="rainfall", type="integer", nullable=false)
     */
    private $rainfall;

    /**
     * @ORM\Column(name="rainfall_category", type="integer", nullable=false)
     */
    private $rainfallCategory;

    /**
     * @ORM\Column(name="rainfall_intensity", type="float", nullable=false)
     */
    private $rainfallIntensity;

    /**
     * @ORM\Column(name="rainfall_since_midnight", type="float", nullable=false)
     */
    private $rainfallSinceMidnight;

    /**
     * @ORM\Column(name="brightness_total", type="float", nullable=false)
     */
    private $brightnessTotal;

    /**
     * @ORM\Column(name="brightness_north", type="float", nullable=false)
     */
    private $brightnessNorth;

    /**
     * @ORM\Column(name="brightness_south", type="float", nullable=false)
     */
    private $brightnessSouth;

    /**
     * @ORM\Column(name="brightness_west", type="float", nullable=false)
     */
    private $brightnessWest;

    /**
     * @ORM\Column(name="brightness_east", type="float", nullable=false)
     */
    private $brightnessEast;

    /**
     * @ORM\Column(name="brightness_direction", type="float", nullable=false)
     */
    private $brightnessDirection;

    /**
     * @ORM\Column(name="longitude", type="float", nullable=false)
     */
    private $longitude;

    /**
     * @ORM\Column(name="latitude", type="float", nullable=false)
     */
    private $latitude;

    /**
     * @ORM\Column(name="sensor_height", type="float", nullable=false)
     */
    private $sensorHeight;

    /**
     * @ORM\Column(name="elavation", type="float", nullable=false)
     */
    private $elavation;

    /**
     * @ORM\Column(name="azimut", type="float", nullable=false)
     */
    private $azimut;

    /**
     * @ORM\Column(name="timestamp", type="datetime", nullable=false)
     */
    private $timestamp;

    public function __construct(
        float $temperature,
        float $windchill,
        float $pressure,
        float $relativeHumidity,
        float $windSpeed,
        int $windDirection,
        int $rainfall,
        int $rainfallCategory,
        float $rainfallIntensity,
        float $rainfallSinceMidnight,
        float $brightnessTotal,
        float $brightnessNorth,
        float $brightnessSouth,
        float $brightnessWest,
        float $brightnessEast,
        float $brightnessDirection,
        float $longitude,
        float $latitude,
        float $sensorHeight,
        float $elavation,
        float $azimut,
        \DateTime $timestamp
    ) {
        $this->temperature = $temperature;
        $this->windchill = $windchill;
        $this->pressure = $pressure;
        $this->relativeHumidity = $relativeHumidity;
        $this->windSpeed = $windSpeed;
        $this->windDirection = $windDirection;
        $this->rainfall = $rainfall;
        $this->rainfallCategory = $rainfallCategory;
        $this->rainfallIntensity = $rainfallIntensity;
        $this->rainfallSinceMidnight = $rainfallSinceMidnight;
        $this->brightnessTotal = $brightnessTotal;
        $this->brightnessNorth = $brightnessNorth;
        $this->brightnessSouth = $brightnessSouth;
        $this->brightnessWest = $brightnessWest;
        $this->brightnessEast = $brightnessEast;
        $this->brightnessDirection = $brightnessDirection;
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        $this->sensorHeight = $sensorHeight;
        $this->elavation = $elavation;
        $this->azimut = $azimut;
        $this->timestamp = $timestamp;
    }

    public static function fromRaspPiInput(array $input)
    {
        return new self(
            (float) $input['temperature'],
            (float) $input['windchill'],
            (float) $input['pressure'],
            (float) $input['relativeHumidity'],
            (float) $input['windSpeed'],
            (int) $input['windDirection'],
            (int) $input['rainfall'],
            (int) $input['rainfallCategory'],
            (float) $input['rainfallIntensity'],
            (float) $input['rainfallSinceMidnight'],
            (float) $input['brightnessTotal'],
            (float) $input['brightnessNorth'],
            (float) $input['brightnessSouth'],
            (float) $input['brightnessWest'],
            (float) $input['brightnessEast'],
            (float) $input['brightnessDirection'],
            (float) $input['longitude'],
            (float) $input['latitude'],
            (float) $input['sensorHeight'],
            (float) $input['elavation'],
            (float) $input['azimut'],
            new \DateTime('@'.$input['timeStamp'])
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getTemperature(): float
    {
        return $this->temperature;
    }

    public function getWindchill(): float
    {
        return $this->windchill;
    }

    public function getPressure(): float
    {
        return $this->pressure;
    }

    public function getRelativeHumidity(): float
    {
        return $this->relativeHumidity;
    }

    public function getWindSpeed(): float
    {
        return $this->windSpeed;
    }

    public function getWindDirection(): int
    {
        return $this->windDirection;
    }

    public function getRainfall(): int
    {
        return $this->rainfall;
    }

    public function getRainfallCategory(): int
    {
        return $this->rainfallCategory;
    }

    public function getRainfallIntensity(): float
    {
        return $this->rainfallIntensity;
    }

    public function getRainfallSinceMidnight(): float
    {
        return $this->rainfallSinceMidnight;
    }

    public function getBrightnessTotal(): float
    {
        return $this->brightnessTotal;
    }

    public function getBrightnessNorth(): float
    {
        return $this->brightnessNorth;
    }

    public function getBrightnessSouth(): float
    {
        return $this->brightnessSouth;
    }

    public function getBrightnessWest(): float
    {
        return $this->brightnessWest;
    }

    public function getBrightnessEast(): float
    {
        return $this->brightnessEast;
    }

    public function getBrightnessDirection(): float
    {
        return $this->brightnessDirection;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getSensorHeight(): float
    {
        return $this->sensorHeight;
    }

    public function getElavation(): float
    {
        return $this->elavation;
    }

    public function getAzimut(): float
    {
        return $this->azimut;
    }

    public function getTimestamp(): string
    {
        return $this->timestamp->format('d.m.Y H:i:s');
    }

}
